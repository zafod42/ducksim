// Паттерн Стратегия определяет семейство алгоритмов,
// инкапсулирует каждый из них и обсеспечивает взаимозаменяемость.
// Он позволяет модифицировать алгоритмы независимо от использования
// их на стороне клиента


public class MiniDuckSimulator
{
    public static void main(String[] args)
    {
        Duck mallard = new MallardDuck();
        mallard.display();
        mallard.performQuack();
        mallard.performFly();

        Duck model = new ModelDuck();
        model.display();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
        model.swim();
        model.setQuackBehavior(new MuteQuack());
        model.performQuack();

        DuckDecoy duckDecoy= new DuckDecoy();
        duckDecoy.display();
        duckDecoy.performQuack();
    }
}
