public abstract class Duck
{
    FlyBehavior fLyBehavior;
    QuackBehavior quackBehavior;

    public Duck()
    {
        // nothing
    }

    public abstract void display();

    public void performFly()
    {
        fLyBehavior.fly();
    }

    public void performQuack()
    {
        quackBehavior.quack();
    }

    public void setFlyBehavior(FlyBehavior fb)
    {
        fLyBehavior = fb;
    }

    public void setQuackBehavior(QuackBehavior qb)
    {
        quackBehavior = qb;
    }
    public void swim()
    {
       System.out.println("All ducks float, even decoys!");
    }
}
