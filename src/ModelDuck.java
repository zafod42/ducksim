public class ModelDuck extends Duck
{
    public ModelDuck()
    {
        fLyBehavior = new FlyNoWay();
        quackBehavior = new Quack();
    }

    public void display()
    {
        System.out.println("I'm a model duck");
    }
}
