public class DuckDecoy
{
    QuackBehavior quackBehavior;
    public DuckDecoy()
    {
        quackBehavior = new Quack();
    }

    public void setQuackBehavior(QuackBehavior qb)
    {
        quackBehavior = qb;
    }

    public void performQuack()
    {
        quackBehavior.quack();
    }

    public void display()
    {
        System.out.println("It's a decoy");
    }
}
